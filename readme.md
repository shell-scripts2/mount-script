# Mount Script
Created a mount script because I hate Linux sometimes.

## Setup
Make python script executable
```
chmod +x mount-tool.py
```
Setup the disk directory
```
./mount-tool.py -s
```

## Mount Script Usage
### Help
Run this command to get help 
```
./mount-tool.py -h
```
Output:
```
-h    help
-d    specify the disk
-m    mount disk
-s    setup disk directory
-u    unmount disk
```

### Mount
Mount the disk
```
./mount-tool.py -m /dev/disk_partition
```

Example:
```
./mount-tool.py -m /dev/sdc1
```

### Unmount
Unmount the disk
```
./mount-tool.py -u /dev/disk_partition
```

Example:
```
./mount-tool.py -u /dev/sdc1 
```

