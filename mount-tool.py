#!/usr/bin/env python3

import os
import sys
import subprocess

def Help():
    print("-h    help")
    print("-d    specify the disk")
    print("-m    mount disk")
    print("-s    setup disk directory")
    print("-u    unmount disk")

disk_path = "/media/" + os.environ['USER'] + "/disk"

try:
    match sys.argv[1]:
        case "-h":
            Help()
        case "-m":
            subprocess.run(["sudo", "mount", "-o", "rw", sys.argv[2], disk_path])
        case "-s":
            subprocess.run(["sudo", "mkdir", disk_path])
        case "-u":
            subprocess.run(["sudo", "umount", sys.argv[2]])
        case _:
            print("Invalid option: pass -h for help")
except IndexError:
    print("Invalid option: pass -h for help")

